#!/bin/bash

read -p "Digite o nome de 3 diretorios separados por espaço: " $1 $2 $3

a=$(( ls -la $1 | grep "^-" | wc -l ))
b=$(( ls -la $2 | grep "^-" | wc -l ))
c=$(( ls -la $3 | grep "^-" | wc -l ))

echo "Total de arquivos contidos nos 3 diretórios: $(( a +b + c ))"
